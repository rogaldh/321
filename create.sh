#!/bin/bash -e

### Example
# create.sh ./path/to/destination/directory archive_name [c|C] [comp|nocomp] [e]

source_name="$2"
archive_name="$source_name.zip"
destination_fullpath="$1/$archive_name"
should_validate_integrity=""
should_validate_with_script=""
should_encrypt=""
should_compress=""

if [[ -z $1 ]]; then echo "Absent target" &1>2; exit 0; fi

if [[ $3 == C ]]; then should_validate_with_script=1; fi
if [[ $3 == c ]]; then should_validate_integrity="-T"; fi
if [[ -z $3 ]]; then should_validate_integrity="-T"; fi

if [[ $5 == e ]]; then should_encrypt="-e"; fi

if [[ $4 == comp ]]; then should_compress="-1"; fi
if [[ $4 == nocomp ]]; then should_compress="-0"; fi
if [[ -z $4 ]]; then should_compress="-0"; fi

if [[ $should_validate_with_script == 1 ]]; then

zip -r $should_compress $should_encrypt $destination_fullpath $source_name
$( dirname -- "$0"; )/check.sh "$destination_fullpath"

else

zip -r $should_compress $should_encrypt $should_validate_integrity $destination_fullpath $source_name

fi
