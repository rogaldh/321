#!/bin/bash -e
if [[ -z $1 ]]; then echo "Absent target" &1>2; exit 0; fi
if [[ -z $2 ]]; then echo "Absent source" &1>2; exit 0; fi
if [[ $3 == e ]]; then
	zip -u$([[ $4 == c ]] && echo "1" || echo "0")re $1 $2
else
	zip -u$([[ $4 == c ]] && echo "1" || echo "0")r $1 $2
fi
if [[ $5 == check ]]; then
	$( dirname -- "$0"; )/check.sh $1
fi
